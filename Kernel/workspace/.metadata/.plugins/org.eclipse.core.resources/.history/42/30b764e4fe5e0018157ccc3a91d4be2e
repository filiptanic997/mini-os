/*
 * Kernel.cpp
 *
 *  Created on: May 23, 2018
 *      Author: Filip Tanic
 */

#include <STDIO.H>
#include "Kernel.h"
#include "Timer.h"

unsigned ttsp = 0u;
unsigned ttss = 0u;
unsigned ttbp = 0u;

void interrupt __KERNEL_MODE(...){//switch to kernel thread, int 61h
	lockFlag = 0;//interrupts are not allowed while setting the flag!
#ifndef BCC_BLOCK_IGNORE
	asm {
		sti
		mov tcx, cx
		mov tdx, dx
		mov ttsp, sp
		mov ttss, ss
		mov ttbp, bp
	}

	globalSYSDATA = (__SYS_DATA*) MK_FP(tcx, tdx);//initializing the global pointer to __SYS_DATA


#endif
	PCB::running->reg.sp = ttsp;
	PCB::running->reg.ss = ttss;
	PCB::running->reg.bp = ttbp;//save running thread's context

	ttsp = PCB::kernel->reg.sp;
	ttss = PCB::kernel->reg.ss;
	ttbp = PCB::kernel->reg.bp;

#ifndef BCC_BLOCK_IGNORE
	asm {
		mov sp, ttsp
		mov ss, ttss
		mov bp, ttbp
	}
#endif
}

intr_routine __K_MODE = __KERNEL_MODE;

void __KERNEL_RUN(){
	int switchContext;
	PCB* targetPCB;
	KernelSem* targetSem;
	KernelEv* targetEv;

	while(1){
		switchContext = 0;
		switch(globalSYSDATA->request){
		case __CREATE_THR:
			//asm cli;
			targetPCB = new PCB(globalSYSDATA->targetID, (Thread*)globalSYSDATA->pointers[0], globalSYSDATA->values[0], globalSYSDATA->values[1]);
			globalPCB.push_back(targetPCB, globalSYSDATA->targetID);
			//asm sti;
			break;

		case __DELETE_THR:
			//asm cli;
			targetPCB = globalPCB.delete_id(globalSYSDATA->targetID);//deletion only
			delete targetPCB;
			//asm sti;
			break;

		case __START_THR:
			targetPCB = globalPCB.find_id(globalSYSDATA->targetID);
			if(targetPCB->currentState == PCB::NEW){
				targetPCB->currentState = PCB::READY;
				Scheduler::put(targetPCB);
			}
			break;

		case __WAIT_THR:
			printf("Kernel Thr has %d\n", PCB::kernel->id);
			printf("PCB::running has %d\n", PCB::running->id);
			targetPCB = globalPCB.find_id(globalSYSDATA->targetID);
			targetPCB->waitToComplete();
			switchContext = 1;
			break;

		case __SLEEP:
			sleepPCB.push_sleep((PCB*)PCB::running, globalSYSDATA->values[0]);
			PCB::running->currentState = PCB::BLOCKED;//static method
			switchContext = 1;
			break;

		case __DISPATCH:
			printf("Dispatch\n");
			switchContext = 1;
		}
		//check if a context switch is needed
		if(switchContext == 1){
			if(PCB::running->currentState != PCB::FINISHED && PCB::running->currentState != PCB::BLOCKED && PCB::running != PCB::idle){

				PCB::running->currentState = PCB::READY;
				Scheduler::put((PCB*)PCB::running);
			}

			PCB* next = Scheduler::get();

			if(next == NULL)
				PCB::running = PCB::idle;
			else
				PCB::running = next;

			PCB::running->currentState = PCB::RUNNING;
			ticksLeft = PCB::running->timeSlice;
		}

#ifndef BCC_BLOCK_IGNORE
		asm int 63h;
#endif
	}
}

void interrupt __USER_MODE(...){//switch back to user thread, int 63h
#ifndef BCC_BLOCK_IGNORE
	asm {
		mov ttsp, sp
		mov ttss, ss
		mov ttbp, bp
	}
#endif

	PCB::kernel->reg.sp = ttsp;
	PCB::kernel->reg.ss = ttss;
	PCB::kernel->reg.bp = ttbp;//save kernel thread's context

	ttsp = PCB::running->reg.sp;
	ttss = PCB::running->reg.ss;
	ttbp = PCB::running->reg.bp;//switch back to running thread's context

#ifndef BCC_BLOCK_IGNORE
	asm {
		mov sp, ttsp
		mov ss, ttss
		mov bp, ttbp
		cli
	}
#endif
	lockFlag = 1;//interrupts are not allowed while setting the flag!
#ifndef BCC_BLOCK_IGNORE
	if(context_switch_requested == 1)
		asm int 8h;
#endif
}

intr_routine __U_MODE = __USER_MODE;

PCB* createKernelPCB(){
	unsigned stackSize = 512 / sizeof(unsigned);
	PCB* kernelPCB = new PCB(-3, NULL, 512, 1);//timeSlice is not of importance
	globalPCB.push_back(kernelPCB, -3);
#ifndef BCC_BLOCK_IGNORE
	kernelPCB->stack[stackSize - 6] = FP_SEG(__KERNEL_RUN);//__KERNEL_RUN
	kernelPCB->stack[stackSize - 7] = FP_OFF(__KERNEL_RUN);//__KERNEL_RUN
#endif
	return kernelPCB;
}

PCB* const PCB::kernel = createKernelPCB();
