/*
 * DList.h
 *
 *  Created on: May 10, 2018
 *      Author: Filip Tanic
 */

#ifndef DLIST_H_
#define DLIST_H_

#include <STDLIB.H>

class PCB;
class KernelSem;
class KernelEv;

class D_List_PCB {
public:
	class Node{
	public:
		PCB *pcb;
		int auxData;
		Node *prev, *next;

		Node(PCB *ppcb, Node *pprev = NULL, Node *nnext = NULL)
			: pcb(ppcb), prev(pprev), next(nnext) {}

		~Node() { pcb = NULL; prev = NULL; next = NULL; }
	};

private:
	Node* head, *tail;
	int size;

public:
	int getSize() const { return size; }

	int decrementAndNotify();//returns non-zero value if there are threads to wake up

	D_List_PCB& push_front(PCB* pcb);
	D_List_PCB& push_back(PCB* pcb);
	D_List_PCB& push_sleep(PCB* pcb, int toSleep = 1);
	PCB* front() const;//returns NULL if empty
	int front_data() const;//returns -1 if empty
	PCB* back() const;//returns NULL if empty
	int back_data() const;//returns -1 if empty
	D_List_PCB& pop_front();
	D_List_PCB& pop_back();

	D_List_PCB() : head(NULL), tail(NULL), size(0) {}
	~D_List_PCB();//potential blind spot
};

class D_List_Sem {
public:
	class Node{
	public:
		KernelSem *pcb;
		int auxData;
		Node *prev, *next;

		Node(KernelSem *ppcb, Node *pprev = NULL, Node *nnext = NULL)
			: pcb(ppcb), prev(pprev), next(nnext) {}

		~Node() { pcb = NULL; prev = NULL; next = NULL; }
	};

private:
	Node* head, *tail;
	int size;

public:
	int getSize() const { return size; }

	D_List_Sem& push_front(KernelSem* pcb);
	D_List_Sem& push_back(KernelSem* pcb);
	KernelSem* front() const;//returns NULL if empty
	int front_data() const;//returns -1 if empty
	KernelSem* back() const;//returns NULL if empty
	int back_data() const;//returns -1 if empty
	D_List_Sem& pop_front();
	D_List_Sem& pop_back();

	D_List_Sem() : head(NULL), tail(NULL), size(0) {}
	~D_List_Sem();//potential blind spot
};

class D_List_Ev {
public:
	class Node{
	public:
		KernelEv *pcb;
		int auxData;
		Node *prev, *next;

		Node(KernelEv *ppcb, Node *pprev = NULL, Node *nnext = NULL)
			: pcb(ppcb), prev(pprev), next(nnext) {}

		~Node() { pcb = NULL; prev = NULL; next = NULL; }
	};

private:
	Node* head, *tail;
	int size;

public:
	int getSize() const { return size; }

	D_List_Ev& push_front(KernelEv* pcb);
	D_List_Ev& push_back(KernelEv* pcb);
	D_List_Ev& push_sleep(KernelEv* pcb, int toSleep = 1);//not implemented
	KernelEv* front() const;//returns NULL if empty
	int front_data() const;//returns -1 if empty
	KernelEv* back() const;//returns NULL if empty
	int back_data() const;//returns -1 if empty
	D_List_Ev& pop_front();
	D_List_Ev& pop_back();

	D_List_Ev() : head(NULL), tail(NULL), size(0) {}
	~D_List_Ev();//potential blind spot
};

#endif /* DLIST_H_ */
