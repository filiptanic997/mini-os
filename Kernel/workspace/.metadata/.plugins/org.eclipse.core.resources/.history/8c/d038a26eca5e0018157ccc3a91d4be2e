/*
 * Thread.h
 *
 *  Created on: May 11, 2018
 *      Author: Filip Tanic
 */

#ifndef THREAD_H_
#define THREAD_H_

#include "PCB.h"

typedef unsigned long StackSize;
const StackSize defaultStackSize = 4096ul;
typedef unsigned int Time;
const Time defaultTimeSlice = 2u;

class Thread {
private:
	ID myID;
	static Thread* idleThread;
	static int idThrGen;

protected:
	Thread(StackSize stackSize = defaultStackSize, Time timeSlice = defaultTimeSlice);
	virtual void run() = 0;

public:
	int getID() const { return myID; }

	void start();
	void waitToComplete();

	//returns NULL if thread is not found
	static Thread* getThreadByID(int id);//tests only

	static void sleep(Time toSleep);//in timer ticks, 55ms each

	virtual ~Thread();
};

void dispatch();

#endif /* THREAD_H_ */
