/*
 * PCB.h
 *
 *  Created on: May 10, 2018
 *      Author: Filip Tanic
 */

#ifndef PCB_H_
#define PCB_H_

#include <DOS.H>
#include "SYSDATA.H"
#include "DList.h"
#include "SCHEDULE.H"

typedef void interrupt (*intr_routine)(...);

extern volatile unsigned int context_switch_requested;
extern volatile unsigned int lockFlag;
extern unsigned int ticksLeft;

#define hard_lock asm pushf; asm cli;
#define hard_unlock asm popf;

#define lock lockFlag = 0;
#define unlock lockFlag = 1; if(context_switch_requested) dispatch();

extern D_List_ globalPCB;
extern D_List sleepPCB;

class Thread;

struct REG{
	unsigned ss;
	unsigned sp;
	unsigned bp;// -16
	unsigned cs;//-6, this is PC segment
	unsigned ip;//-7, this is PC offset
	//these 5 need to be set during the initialization of PCB
	/*unsigned ax;// -8, random
	unsigned bx;// -9, random
	unsigned cx;// -10, random
	unsigned dx;// -11, random
	unsigned es;// -12, random
	unsigned ds;// -13, random
	unsigned si;// -14, random
	unsigned di;// -15, random*/
};

class PCB {
private:
	static void wrapper(Thread* toRun);
	void releaseBlocked();

	friend void createMainPCB();
	friend PCB* createIdlePCB();

	PCB();//thread-safe only when called from createMainPCB

public:
	static volatile PCB* running;
	static PCB* const idle;
	D_List_PCB blocked;
	Thread* myThread;

	enum State { NEW, READY, RUNNING, BLOCKED, FINISHED };

	unsigned *stack;
	REG reg;
	unsigned timeSlice;
	State currentState;

	void wait();//real wait, returns a value through __SYSDATA
	void signal();

	PCB(Thread* const thread, unsigned stackSize, unsigned ttimeSlice);//thread-safe only when called from Thread
	~PCB();//thread-safe only when called from Thread
};

void dispatch();

#endif /* PCB_H_ */
