/*
 * Event.h
 *
 *  Created on: May 21, 2018
 *      Author: Filip Tanic
 */

#ifndef EVENT_H_
#define EVENT_H_

#include "PCB.h"

typedef unsigned char IVTNo;

class KernelEv;
class IVTEntry;//for now

#define PREPAREENTRY(entry, callOld)\
void interrupt interrupt_routine_##entry(...);\
IVTEntry entry_##entry(entry, interrupt_routine_##entry);\
void interrupt interrupt_routine_##entry(...){\
	entry_##entry.signal();\
	if(callOld)\
		entry_##entry.callOld();\
}\

class Event {
private:
	KernelEv* myImpl;

protected:
	friend class KernelEv;
	void signal();

public:
	void wait();

	Event(IVTNo ivtNo);
	~Event();
};

class IVTEntry{
private:
	IVTNo myEntry;
	intr_routine old_routine;
	KernelEv* myKernelEv;

	friend class KernelEv;

public:
	void signal();
	void callOld() const;

	static IVTEntry* getEntryNo(IVTNo entry);

	IVTEntry(IVTNo entry, intr_routine interrupt_routine);
	~IVTEntry();
};

#endif /* EVENT_H_ */
