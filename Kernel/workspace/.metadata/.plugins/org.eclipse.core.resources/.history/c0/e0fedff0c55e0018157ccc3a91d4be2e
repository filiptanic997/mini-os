/*
 * DList.cpp
 *
 *  Created on: May 10, 2018
 *      Author: Filip Tanic
 */

#include "../Include/DList.h"

//PCB list

int D_List_PCB::decrementAndNotify(){
	if(head == NULL)
		return 0;

	head->auxData--;

	return head->auxData == 0;
}

D_List_PCB& D_List_PCB::push_front(PCB* pcb, int id){
	if(head == NULL){
		head = tail = new Node(pcb);
	}
	else{
		head->prev = new Node(pcb, NULL, head);
		head = head->prev;
	}
	head->auxData = id;
	size++;
	return *this;
}

D_List_PCB& D_List_PCB::push_back(PCB* pcb, int id){
	if(head == NULL){
		head = tail = new Node(pcb);
	}
	else{
		tail->next = new Node(pcb, tail);
		tail = tail->next;
	}
	tail->auxData = id;
	size++;
	return *this;
}

D_List_PCB& D_List_PCB::push_sleep(PCB* pcb, int toSleep){
	Node* newNode = new Node(pcb);
	newNode->auxData = toSleep;

	if(head == NULL){
		head = tail = newNode;
		size++;
		return *this;
	}

	Node* cur = head, *prev = NULL;
	while(cur != NULL){
		if(newNode->auxData <= cur->auxData){//found the spot
			if(prev != NULL)
				prev->next = newNode;
			else
				head = newNode;

			newNode->prev = prev;
			newNode->next = cur;
			cur->prev = newNode;
			//update the rest of the list
			cur->auxData -= newNode->auxData;

			size++;
			return *this;
		}
		else{
			newNode->auxData -= cur->auxData;
			prev = cur;
			cur = cur->next;
		}
	}

	if(cur == NULL){
		newNode->prev == tail;
		tail = tail->next = newNode;
	}

	size++;
	return *this;
}

PCB* D_List_PCB::front() const{
	if(head != NULL)
		return head->pcb;

	return NULL;
}

int D_List_PCB::front_data() const{//returns -1 if the list is empty
	if(head != NULL)
		return head->auxData;

	return -1;
}

PCB* D_List_PCB::back() const{
	if(tail != NULL)
		return tail->pcb;

	return NULL;
}

int D_List_PCB::back_data() const{
	if(tail != NULL)
		return tail->auxData;

	return -1;
}

PCB* PCB::find_id(ID id){
	Node* cur = head;
	while(cur != NULL){
		if(cur->auxData == id)
			return cur->pcb;

		cur = cur->next;
	}

	return NULL;
}

PCB* delete_id(ID id);

D_List_PCB& D_List_PCB::pop_front(){
	if(head == NULL)
		return *this;

	Node* oldHead = head;
	head = head->next;
	if(head == NULL)
		tail = NULL;

	delete oldHead;
	size--;
	return *this;
}

D_List_PCB& D_List_PCB::pop_back(){
	if(head == NULL)
		return *this;

	Node* oldTail = tail;
	tail = tail->prev;
	if(tail == NULL)
		head = NULL;

	delete oldTail;
	size--;
	return *this;
}

D_List_PCB::~D_List_PCB() {
	Node *cur = head;

	while(cur != NULL){
		Node *prev = cur;
		cur = cur->next;
		delete prev;
	}

	size = 0;
	head = tail = NULL;
}

//KernelSem list

D_List_Sem& D_List_Sem::push_front(KernelSem* sem, int id){
	if(head == NULL){
		head = tail = new Node(sem);
	}
	else{
		head->prev = new Node(sem, NULL, head);
		head = head->prev;
	}
	head->auxData = id;
	size++;
	return *this;
}

D_List_Sem& D_List_Sem::push_back(KernelSem* sem, int id){
	if(head == NULL){
		head = tail = new Node(sem);
	}
	else{
		tail->next = new Node(sem, tail);
		tail = tail->next;
	}
	tail->auxData = id;
	size++;
	return *this;
}

KernelSem* D_List_Sem::front() const{
	if(head != NULL)
		return head->sem;

	return NULL;
}

int D_List_Sem::front_data() const{//returns -1 if the list is empty
	if(head != NULL)
		return head->auxData;

	return -1;
}

KernelSem* D_List_Sem::back() const{
	if(tail != NULL)
		return tail->sem;

	return NULL;
}

int D_List_Sem::back_data() const{
	if(tail != NULL)
		return tail->auxData;

	return -1;
}

D_List_Sem& D_List_Sem::pop_front(){
	if(head == NULL)
		return *this;

	Node* oldHead = head;
	head = head->next;
	if(head == NULL)
		tail = NULL;

	delete oldHead;
	size--;
	return *this;
}

D_List_Sem& D_List_Sem::pop_back(){
	if(head == NULL)
		return *this;

	Node* oldTail = tail;
	tail = tail->prev;
	if(tail == NULL)
		head = NULL;

	delete oldTail;
	size--;
	return *this;
}

D_List_PCB::~D_List_PCB() {
	Node *cur = head;

	while(cur != NULL){
		Node *prev = cur;
		cur = cur->next;
		delete prev;
	}

	size = 0;
	head = tail = NULL;
}

//KernelEv list

D_List_Ev& D_List_Ev::push_front(KernelEv* ev, int id){
	if(head == NULL){
		head = tail = new Node(ev);
	}
	else{
		head->prev = new Node(ev, NULL, head);
		head = head->prev;
	}
	head->auxData = id;
	size++;
	return *this;
}

D_List_Ev& D_List_Ev::push_back(KernelEv* ev, int id){
	if(head == NULL){
		head = tail = new Node(ev);
	}
	else{
		tail->next = new Node(ev, tail);
		tail = tail->next;
	}
	tail->auxData = id;
	size++;
	return *this;
}

KernelEv* D_List_Ev::front() const{
	if(head != NULL)
		return head->ev;

	return NULL;
}

int D_List_Ev::front_data() const{//returns -1 if the list is empty
	if(head != NULL)
		return head->auxData;

	return -1;
}

KernelEv* D_List_Ev::back() const{
	if(tail != NULL)
		return tail->ev;

	return NULL;
}

int D_List_Ev::back_data() const{
	if(tail != NULL)
		return tail->auxData;

	return -1;
}

D_List_Ev& D_List_Ev::pop_front(){
	if(head == NULL)
		return *this;

	Node* oldHead = head;
	head = head->next;
	if(head == NULL)
		tail = NULL;

	delete oldHead;
	size--;
	return *this;
}

D_List_Ev& D_List_Ev::pop_back(){
	if(head == NULL)
		return *this;

	Node* oldTail = tail;
	tail = tail->prev;
	if(tail == NULL)
		head = NULL;

	delete oldTail;
	size--;
	return *this;
}

D_List_PCB::~D_List_PCB() {
	Node *cur = head;

	while(cur != NULL){
		Node *prev = cur;
		cur = cur->next;
		delete prev;
	}

	size = 0;
	head = tail = NULL;
}
