/*
 * PCB.cpp
 *
 *  Created on: May 10, 2018
 *      Author: Filip Tanic
 */

#include "../Include/PCB.h"

#include <IOSTREAM.H>
#include <STDIO.H>
#include "../Include/Thread.h"

volatile unsigned int context_switch_requested = 0;
volatile unsigned int lockFlag = 1;
unsigned int ticksLeft = 5u;//Time
volatile PCB* PCB::running = NULL;
D_List_PCB globalPCB;
D_List_PCB sleepPCB;

void PCB::wrapper(Thread* toRun){
	PCB* thisPCB = (PCB*) PCB::running;
	toRun->run();

	lock;//perhaps unnecessary?
	thisPCB->releaseBlocked();
	thisPCB->currentState = FINISHED;
	unlock;

	dispatch();
}

void PCB::releaseBlocked(){//private method, not implicitly thread-safe!
	//lock;
	PCB* b;
	while((b = blocked.front()) != NULL){
		blocked.pop_front();
		b->currentState = READY;
		Scheduler::put(b);
	}
	//unlock;
}

void PCB::waitToComplete(){
	if(currentState == FINISHED){
		return;
	}

	blocked.push_back((PCB*)PCB::running, NULL);//second argument is not needed
	PCB::running->currentState = BLOCKED;
	context_switch_requested = 1;//check this
}

PCB::PCB() {//main thread has a null pointer!!!
	//lock;
	unsigned stackSize = 65535 / sizeof(unsigned);
	stack = new unsigned[stackSize];
	timeSlice = 5;
#ifndef BCC_BLOCK_IGNORE
	reg.ss = FP_SEG(stack + stackSize);
	reg.sp = FP_OFF(stack + stackSize);
	reg.bp = reg.sp;
#endif
	currentState = RUNNING;
	//unlock;
}

PCB::PCB(Thread* const thread, unsigned stackSize, unsigned ttimeSlice)
	: timeSlice(ttimeSlice) {
	//lock;
	if(stackSize > 65535)//from 5 to 3, check!
		stackSize = 65535;

	stackSize /= sizeof(unsigned);
#ifndef BCC_BLOCK_IGNORE
	stack = new unsigned[stackSize];

	//stack initialization
	stack[stackSize - 1] = FP_SEG(thread);//parameter
	stack[stackSize - 2] = FP_OFF(thread);//parameter
	stack[stackSize - 5] = 0x200;//PSW, I bit enabled only
	stack[stackSize - 6] = FP_SEG(PCB::wrapper);//wrapper
	stack[stackSize - 7] = FP_OFF(PCB::wrapper);//wrapper

	reg.ss = FP_SEG(stack + stackSize - 16);
	reg.sp = FP_OFF(stack + stackSize - 16);
	reg.bp = reg.sp;
#endif
	currentState = NEW;
	//unlock;
}

PCB::~PCB() {
	//lock
	delete stack;
	//unlock
}

void dispatch(){
	hard_lock;
	context_switch_requested = 1;
	asm int 8h;
	hard_unlock;
}

void createMainPCB(){
	lock;
	PCB* mainPCB = new PCB();
	globalPCB.push_back(mainPCB, -1);
	ticksLeft = mainPCB->timeSlice;
	PCB::running = mainPCB;
	unlock;//is it enough
}

void loop(){
	while(1);
}

PCB* createIdlePCB(){
	lock;
	unsigned stackSize = 100 / sizeof(unsigned);
	PCB* idlePCB = new PCB(NULL, 100, 1);
	globalPCB.push_back(idlePCB, -2);
#ifndef BCC_BLOCK_IGNORE
	//overriding wrapper with loop function
	idlePCB->stack[stackSize - 4] = FP_SEG(loop);//loop
	idlePCB->stack[stackSize - 5] = FP_OFF(loop);//loop
#endif
	unlock;//is it enough

	return idlePCB;
}

PCB* const PCB::idle = createIdlePCB();
