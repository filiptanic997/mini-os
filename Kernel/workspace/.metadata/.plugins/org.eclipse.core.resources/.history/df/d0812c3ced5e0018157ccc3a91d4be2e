/*
 * Kernel.cpp
 *
 *  Created on: May 23, 2018
 *      Author: Filip Tanic
 */

#include <STDIO.H>
#include "Kernel.h"

extern unsigned tsp;//check this!
extern unsigned tss;
extern unsigned tbp;

void interrupt __KERNEL_MODE(...){//switch to kernel thread, int 61h
	lockFlag = 0;//interrupts are not allowed while setting the flag!
#ifndef BCC_BLOCK_IGNORE
	asm{
		sti
		mov tcx, cx
		mov tdx, dx
	}

	globalSYSDATA = (__SYS_DATA*) MK_FP(tcx, tdx);//initializing the global pointer to __SYS_DATA

	asm {
		mov tsp, sp
		mov tss, ss
		mov tbp, bp
	}
#endif
	PCB::running->reg.sp = tsp;
	PCB::running->reg.ss = tss;
	PCB::running->reg.bp = tbp;//save running thread's context

	printf("Sta je ovo? 61, globalSYSDATA->targetID = %d\n", globalSYSDATA->targetID);//debug only

	tsp = PCB::kernel->reg.sp;
	tss = PCB::kernel->reg.ss;
	tbp = PCB::kernel->reg.bp;//switch to kernel thread's context

	printf("tsp = %d, tss = %d, tbp = %d\n", tsp, tss, tbp);

#ifndef BCC_BLOCK_IGNORE
	asm {
		mov sp, tsp
		mov ss, tss
		mov bp, tbp
	}
#endif
}

intr_routine __K_MODE = __KERNEL_MODE;

void __KERNEL_RUN(){
	int switchContext;
	PCB* targetPCB;
	KernelSem* targetSem;
	KernelEv* targetEv;

	while(1){
		printf("Svic u kernelu!\n");//debug only
		switchContext = 0;
		switch(globalSYSDATA->request){
		case __CREATE_THR:
			//asm cli;
			targetPCB = new PCB((Thread*)globalSYSDATA->pointers[0], globalSYSDATA->values[0], globalSYSDATA->values[1]);
			globalPCB.push_back(targetPCB, globalSYSDATA->targetID);
			//asm sti;
			break;

		case __DELETE_THR:
			//asm cli;
			targetPCB = globalPCB.delete_id(globalSYSDATA->targetID);//deletion only
			delete targetPCB;
			//asm sti;
			break;

		case __START_THR:
			targetPCB = globalPCB.find_id(globalSYSDATA->targetID);
			if(targetPCB->currentState == PCB::NEW){
				targetPCB->currentState = PCB::READY;
				Scheduler::put(targetPCB);
			}
			break;

		case __WAIT_THR:
			targetPCB = globalPCB.find_id(globalSYSDATA->targetID);
			targetPCB->waitToComplete();
			switchContext = 1;
			break;

		case __SLEEP:
			sleepPCB.push_sleep((PCB*)PCB::running, globalSYSDATA->values[0]);
			PCB::running->currentState = PCB::BLOCKED;//static method
			switchContext = 1;
			break;

		case __DISPATCH:
			switchContext = 1;
		}
		//check if a context switch is needed
		if(switchContext == 1){
			if(PCB::running->currentState != PCB::FINISHED && PCB::running->currentState != PCB::BLOCKED && PCB::running != PCB::idle){

				PCB::running->currentState = PCB::READY;
				Scheduler::put((PCB*)PCB::running);
			}

			PCB* next = Scheduler::get();

			if(next == NULL)
				PCB::running = PCB::idle;
			else
				PCB::running = next;

			PCB::running->currentState = PCB::RUNNING;
			ticksLeft = PCB::running->timeSlice;
		}

#ifndef BCC_BLOCK_IGNORE
		asm int 63h;
#endif
	}
}

void interrupt __USER_MODE(...){//switch back to user thread, int 63h
#ifndef BCC_BLOCK_IGNORE
	asm {
		mov tsp, sp
		mov tss, ss
		mov tbp, bp
	}
#endif

	printf("Prekiddddddddddddddddddddddd 63\n");

	PCB::kernel->reg.sp = tsp;
	PCB::kernel->reg.ss = tss;
	PCB::kernel->reg.bp = tbp;//save kernel thread's context

	PCB::running->reg.sp = tsp;
	PCB::running->reg.ss = tss;
	PCB::running->reg.bp = tbp;//switch back to running thread's context

#ifndef BCC_BLOCK_IGNORE
	asm {
		mov sp, tsp
		mov ss, tss
		mov bp, tbp
		cli
	}
#endif
	lockFlag = 1;//interrupts are not allowed while setting the flag!
#ifndef BCC_BLOCK_IGNORE
	if(context_switch_requested == 1)
		asm int 8h;
#endif
}

intr_routine __U_MODE = __USER_MODE;

PCB* createKernelPCB(){
	printf("Konstruktor Kernel Thr\n");//debug only
	unsigned stackSize = 512 / sizeof(unsigned);
	PCB* kernelPCB = new PCB(NULL, 512, 1);//timeSlice is not of importance
	globalPCB.push_back(kernelPCB, -3);
#ifndef BCC_BLOCK_IGNORE
	kernelPCB->stack[stackSize - 6] = FP_SEG(__KERNEL_RUN);//__KERNEL_RUN
	kernelPCB->stack[stackSize - 7] = FP_OFF(__KERNEL_RUN);//__KERNEL_RUN
#endif
	printf("Konstruktor Kernel Thr, tsp = tss = tbp = %d\n", kernelPCB->reg.sp);
#ifndef BCC_BLOCK_IGNORE
	(MK_FP(tcx, tdx))();
#endif
	return kernelPCB;
}

PCB* const PCB::kernel = createKernelPCB();
