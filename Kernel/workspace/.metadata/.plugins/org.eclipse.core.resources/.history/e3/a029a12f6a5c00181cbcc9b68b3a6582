/*
 * KernlSem.cpp
 *
 *  Created on: May 20, 2018
 *      Author: Filip Tanic
 */

#include "../Include/KernlSem.h"

void KernelSem::releaseWaiting(){//private method, not implicitly thread-safe!
	PCB* b;
	while((b = waiting.front()) != NULL){
		waiting.pop_front();
		b->currentState = PCB::READY;
		Scheduler::put(b);
	}
}

int KernelSem::val() const{
	return semVal;
}

int KernelSem::wait(int toBlock){
	int toRet = 0;
	hard_lock;
	if(toBlock != 0){//standard behavior
		if(--semVal < 0){
			waiting.push_back((PCB*)PCB::running);
			PCB::running->currentState = PCB::BLOCKED;
			dispatch();
			toRet = 1;
		}
	}
	else{//toBlock != 0
		if(semVal <= 0){
			toRet = -1;
		}
		else{//semVal > 0
			semVal--;
		}
	}
	hard_unlock;
	return toRet;
}

void KernelSem::signal(){
	hard_lock;
	if(semValal++ <= 0){//release a single thread
		PCB* notified = waiting.front();
		waiting.pop_front();
		notified->currentState = PCB::READY;
		Scheduler::put(notified);
	}
	hard_unlock;
}

KernelSem::KernelSem(int initialValue = 1) : semVal(initialValue) {}//thread-safe only if called from Semaphore!

KernelSem::~KernelSem(){//thread-safe only if called from Semaphore!
	releaseWaiting();
}
