/*
 * Thread.h
 *
 *  Created on: May 11, 2018
 *      Author: Filip Tanic
 */

#ifndef THREAD_H_
#define THREAD_H_

#include "PCB.h"
#include "SCHEDULE.h"

typedef unsigned long StackSize;
const StackSize defaultStackSize = 32768ul;
typedef unsigned int Time;
const Time defaultTimeSlice = 1u;

class Thread {
private:
	PCB* myPCB;
	static Thread* idleThread;
	static int idGen;
	int myID;//for later use

protected:
	friend class PCB;
	Thread(StackSize stackSize = defaultStackSize, Time timeSlice = defaultTimeSlice);
	virtual void run() = 0;

public:
	static PCB* getIdle();
	int getID() const { return myID; }

	void start();
	void waitToComplete();

	//returns NULL if thread is not found
	static Thread* getThreadByID(int id);//tests only

	static void sleep(Time toSleep);//in timer ticks, 55ms each

	virtual ~Thread();
};

void dispatch();

#endif /* THREAD_H_ */
