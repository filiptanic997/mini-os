/*
 * Event.h
 *
 *  Created on: May 21, 2018
 *      Author: Filip Tanic
 */

#ifndef EVENT_H_
#define EVENT_H_

typedef unsigned char IVTNo;
typedef int ID;

class IVTEntry;

#define PREPAREENTRY(entry, toCallOld)\
void interrupt interrupt_routine_##entry(...);\
IVTEntry entry_##entry(entry, interrupt_routine_##entry);\
void interrupt interrupt_routine_##entry(...){\
	entry_##entry.signal();\
	if(toCallOld){\
		entry_##entry.callOld();\
	}\
}

class Event {
private:
	ID myID;

protected:
	void signal();

public:
	void wait();

	Event(IVTNo ivtNo);
	~Event();
};

#endif /* EVENT_H_ */
