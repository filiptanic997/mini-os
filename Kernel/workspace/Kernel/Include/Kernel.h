/*
 * Kernel.h
 *
 *  Created on: May 24, 2018
 *      Author: Filip Tanic
 */

#ifndef INCLUDE_KERNEL_H_
#define INCLUDE_KERNEL_H_

#include "PCB.h"
#include "KernlSem.h"
#include "KernlEv.h"

extern intr_routine __K_MODE;
extern intr_routine __U_MODE;

#endif /* INCLUDE_KERNEL_H_ */
