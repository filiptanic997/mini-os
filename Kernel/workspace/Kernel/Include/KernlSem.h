/*
 * KernlSem.h
 *
 *  Created on: May 20, 2018
 *      Author: Filip Tanic
 */

#ifndef KERNELSEM_H_
#define KERNELSEM_H_

#include "Semaphor.h"
#include "PCB.h"

class KernelSem {
private:
	int semVal;
	D_List_PCB waiting;

	void releaseWaiting();

public:
	int val() const;

	int wait(int toBlock);
	void signal();

	KernelSem(int initialValue = 1);//thread-safe only if called from Semaphore!
	~KernelSem();
};

#endif /* KERNELSEM_H_ */
