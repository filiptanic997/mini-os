/*
 * Semaphor.h
 *
 *  Created on: May 20, 2018
 *      Author: Filip Tanic
 */

#ifndef SEMAPHOR_H_
#define SEMAPHOR_H_

typedef int ID;

class Semaphore {
private:
	ID myID;

public:
	int val() const;

	virtual int wait(int toBlock);
	virtual void signal();

	Semaphore(int initialValue = 1);//mutex by default
	virtual ~Semaphore();
};

#endif /* SEMAPHOR_H_ */
