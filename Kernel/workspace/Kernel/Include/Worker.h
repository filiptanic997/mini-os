/*
 * Worker.h
 *
 *  Created on: May 19, 2018
 *      Author: Filip Tanicss
 */

#ifndef WORKER_H_
#define WORKER_H_

#include "Thread.h"
#include "Semaphor.h"
#include "Event.h"

class Worker : public Thread {
private:
	//Event* myEvent;
	long myNumber;

	void myMethod();

public:
	void run() {
		myMethod();
	}

	Worker(long myNum);
};

#endif /* WORKER_H_ */
