/*
 * KernlEve.cpp
 *
 *  Created on: May 21, 2018
 *      Author: Filip Tanic
 */

#include "KernlEv.h"
#include "PCB.h"

IVTEntry* IVTEntries[256] = { NULL };

void KernelEv::wait(){
	/*hard_lock;
	if(semVal == 1)
		semVal = 0;
	else{
		waiting = (PCB*)PCB::running;
		waiting->currentState = PCB::BLOCKED;
		dispatch();
	}
	hard_unlock;*/
}

void KernelEv::signal(){
	/*hard_lock;
	if(waiting != NULL){
		waiting->currentState = PCB::READY;
		Scheduler::put(waiting);
		waiting = NULL;
	}
	else//waiting == NULL
		semVal = 1;
	hard_unlock;*/
}

KernelEv::KernelEv(IVTNo ivtNo) : myEntry(ivtNo), semVal(0) {
	/*hard_lock;
	if(IVTEntries[ivtNo] == NULL)
		exit(1);

	IVTEntries[ivtNo]->myKernelEv = this;
	hard_unlock;*/
}

KernelEv::~KernelEv(){
	/*hard_lock;
	if(IVTEntries[myEntry] != NULL)
		IVTEntries[myEntry]->myKernelEv = NULL;

	signal();
	hard_unlock;*/
}

void IVTEntry::signal(){
	/*hard_lock;
	if(myKernelEv == NULL)
		exit(1);

	myKernelEv->signal();
	hard_unlock;*/
}

void IVTEntry::callOld() const{
	old_routine();
}

IVTEntry* IVTEntry::getEntryNo(IVTNo entry){
	return IVTEntries[entry];
}

IVTEntry::IVTEntry(IVTNo entry, intr_routine interrupt_routine) : myEntry(entry), myKernelEv(NULL) {
#ifndef BCC_BLOCK_IGNORE
	old_routine = getvect(entry);
	setvect(entry, interrupt_routine);
#endif
	IVTEntries[entry] = this;
}

IVTEntry::~IVTEntry() {
#ifndef BCC_BLOCK_IGNORE
	setvect(myEntry, old_routine);
#endif
	IVTEntries[myEntry] = NULL;
}
