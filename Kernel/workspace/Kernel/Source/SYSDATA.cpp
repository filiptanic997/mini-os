/*
 * __SYS_DATA.cpp
 *
 *  Created on: May 23, 2018
 *      Author: Filip Tanic
 */


#include <STDLIB.H>
#include "SYSDATA.H"

volatile unsigned int lockFlag = 1;

unsigned tcx;
unsigned tdx;

__SYS_DATA* globalSYSDATA = NULL;
