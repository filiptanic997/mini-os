/*
 * Semaphor.cpp
 *
 *  Created on: May 20, 2018
 *      Author: Filip Tanic
 */

#include "Semaphor.h"
#include "KernlSem.h"

int Semaphore::val() const{//locked section?
	/*hard_lock;
	int toRet = myImpl->val();
	hard_unlock;
	return toRet;*/
}

int Semaphore::wait(int toBlock){
	/*int toRet;
	hard_lock;
	toRet = myImpl->wait(toBlock);
	hard_unlock;
	return toRet;*/
}

void Semaphore::signal(){
	/*hard_lock;
	myImpl->signal();
	hard_unlock;*/
}

Semaphore::Semaphore(int initialValue){
	/*hard_lock;
	myImpl = new KernelSem(initialValue);
	hard_unlock;*/
}
Semaphore::~Semaphore(){
	/*hard_lock;
	delete myImpl;
	hard_unlock;*/
}
